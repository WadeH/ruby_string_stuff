# Console Color Text Support
# Provides 'color' method that can wrap strings in formatting to display color text in terminal
# Uses ANSI escape codes
# @author Wade Harkins <vdtdev@gmail.com>

class String
  # Color constants, 30-37
  CBLACK = 30
  CRED = 31
  CGREEN = 32
  CBROWN = 33
  CBLUE = 34
  CPURPLE = 35
  CCYAN = 36
  CGRAY = 37
  # Modifiers, controls whether 0 or 1 prepends color value in '[(modifier);(color)m'
  # Add to color constant to indicate a 'dark' color, same as excluding modifier
  CDARK = 0
  # Add to color constant to indicate 'light' color, subtracted from base and used for 1
  CLIGHT = 10
  ##
  # Returns string with specified console color information
  # @param [Number] color The color to use for text, using C(COLOR) consts + CLIGHT or CDARK
  # @param [Hash] options The options hash
  # @option options [Boolean] :reset Whether or not to close returning to orignal color (default false)
  # @option options [Number] :original Color to revert to when resetting, (default is CGRAY)
  # @return [String] source string with specified color tags
  # @example
  #   puts "Hello, World".color(String::CGREEN + String::CLIGHT, reset: true)
  #   > "\033[1;32mHello, World\033[0;37m" (Light green text returning to gray)
  def color(color, options = {})
    reset = options.fetch(:reset, false)
    original = options.fetch(:original, String::CGRAY)

    color_parse = Proc.new do |c|
      cmod = (c > 37)? "1" : "0"
      ccol = (c > 37)? (c - 10).to_s : c.to_s
      "\033[#{cmod};#{ccol}m"
    end

    cstart = color_parse.call(color)
    cend = (reset)? color_parse.call(original) : ""

    "#{cstart}#{self}#{cend}"
  end
end