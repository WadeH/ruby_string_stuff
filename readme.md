# Ruby String Stuff

_String formatting and manipulation scripts_

## Gems

- Uses `rspec` for code tests

## Included scripts

- __Console Color Text__ (`console_color_text.rb`)

	Adds `color` method to `String` class that can wrap text in ANSI escape codes for coloring text in bash console

