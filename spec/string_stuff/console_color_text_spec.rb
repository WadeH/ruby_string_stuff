require 'spec_helper'
RSpec.describe String do
  context "Console color method" do
    it "Adds light green prefix only" do
      str = "Hello".color(String::CGREEN + String::CLIGHT)
      expect(str).to eq("\033[1;32mHello")
    end
    it "Adds light green prefix and resets to gray" do
      str = "Hello".color(String::CGREEN + String::CLIGHT, reset: true)
      expect(str).to eq("\033[1;32mHello\033[0;37m")
    end
  end
end